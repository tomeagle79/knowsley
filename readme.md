# In vscode install IIS Express

Then run Ctrl + F5 to serve the site
Shift + F5 to stop IIS
Restart Ctrl + Shift + F5

# Icons for grid editors, doctypes, etc

https://nicbell.github.io/ucreate/icons.html

# Modernzir config:

https://modernizr.com/download/?csstransforms3d-csstransitions-domprefixes-prefixed-prefixes-setclasses-shiv-testallprops-testprop-teststyles