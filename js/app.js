window.$ = window.jQuery = require('jquery');
require('jquery-validation');
require('jquery-validation-unobtrusive');
window.Popper = require('popper.js').default;
require('bootstrap');
require('slick-carousel/slick/slick.js');

// Modernizr
import ModernizrPackage from 'modernizr'
global.Modernizr = ModernizrPackage

// AOS
import AOS from 'aos';

window.AOS = AOS;

window.AOS.init({
    duration: 750,
    ease: 'ease-in-out'
});


require('./scripts/audio.js');
require('./scripts/video.js');
require('./scripts/scroll.js');
require('./scripts/scrollToBlog.js');
require('./scripts/toggle.js');
require('./scripts/sticky.js');
require('./scripts/quotesPadding.js');
require('./scripts/instagram.js');

// Vendor 
require('./scripts/vendor/arrayFromPolyfill.js');
require('./scripts/vendor/jquery.ba-cond.min.js');
require('./scripts/vendor/jquery.dlmenu.js');
require('./scripts/vendor/jquery.raptorize.1.0.js');
require('./scripts/vendor/jquery.slitslider.js');

// Init
require('./scripts/raptorInit.js');
require('./scripts/slitInit.js');
require('./scripts/slickInit.js');
require('./scripts/dlInit.js');


// Multi-level menu

// Raptor animation on newsletter submit
require('./scripts/userAgent.js');

// Cookie consent

require('cookieconsent');
