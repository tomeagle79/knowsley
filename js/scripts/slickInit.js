// General carousels

jQuery(document).ready(function ($) {
    $slick_defaults = {
        autoplay: false,
        slidesToShow: 1,
        arrows: true,
        dots: true,
        centerMode: false,
        centerPadding: '',
    }
    $('.carousel').each(function () {
        var $carousel = $(this);
        if( $carousel.data('slick-options') !== "" ){
            if(typeof $carousel.data('slick-options') == "string") {
                var $slick_options = JSON.parse( $carousel.data('slick-options') );
            } else {
                var $slick_options = $carousel.data('slick-options')
            }
            
            $carousel.slick($slick_options)
                AOS.refresh(); // Required to for AOS to re-run due to bug in AOS
            } else {
                $carousel.slick($slick_defaults);
            }
        })

    $(window).on('load resize orientationchange', function () {
        var $carousel = $('.memberships .card-deck');
        if ($(window).outerWidth() >= 768) {
            if ($carousel.hasClass('slick-initialized')) {
                $carousel.slick('unslick');
            }
        } else {
            if (!$carousel.hasClass('slick-initialized')) {
                $carousel.slick({
                    infinite: false,
                    arrows: false,
                    dots: true,
                    slidesToShow: 1,
                    centerMode: true,
                    centerPadding: '30px',
                    mobileFirst: true,
                    initialSlide: 1
                });
            }
        } // else
    }) // end window load
});