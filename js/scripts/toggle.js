
// Toggle subnav menus on mobile
function toggleThis() {
    $(this).parents().children('.subnav').slideToggle();
    $(this).toggleClass('open');
}

$('.has-subnav span').on('click', toggleThis);

$('.search-trigger').on('click',function(e){
    e.preventDefault();
    $('#mobile-search-form').toggleClass('open-search');
    $('#search-input').focus();
});

