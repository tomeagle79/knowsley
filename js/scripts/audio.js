// Custom controls for animal sound players

const players = document.querySelectorAll('.player');

if(players.length >= 1){
    function formatTime(time)
    {   
        // Hours, minutes and seconds
        var hrs = ~~(time / 3600);
        var mins = ~~((time % 3600) / 60);
        var secs = ~~time % 60;
    
        // Output like "1:01" or "4:03:59" or "123:03:59"
        var ret = "";
    
        if (hrs > 0) {
            ret += "" + hrs + ":" + (mins < 10 ? "0" : "");
        }
    
        ret += "" + mins + ":" + (secs < 10 ? "0" : "");
        ret += "" + secs;
        return ret;
    }
    
    Array.from(players).forEach(function (player) {
    
        const audio = player.querySelector('audio');
        const currentTime = player.querySelector('.current__time');
        const totalTime = player.querySelector('.total__time');
        totalTime.innerHTML = formatTime(audio.duration);
        // audio.addEventListener('loadedmetadata', function() {
        // });
    
        const progress = player.querySelector('.progress');
        const progressBar = player.querySelector('.progress__filled');
        const toggle = player.querySelector('.toggle');
        const mute = player.querySelector('.mute');
        const ranges = player.querySelectorAll('.player__slider');
    
    
        // Build functions
    
        function togglePlay() {
            if (audio.paused) {
                audio.play();
            } else {
                audio.pause();
            }
        }
    
        function toggleSound(e) {
            e = e || window.event;
            audio.muted = !audio.muted;
            e.preventDefault();
    
            const icon = audio.muted ? '<img src="/images/icons/volume-off.svg" alt="off">' : '<img src="/images/icons/volume.svg" alt="on">';
            mute.innerHTML = icon;
        }
    
        function updateButton() {
            const icon = this.paused ? '►' : '❚❚';
            toggle.innerText = icon;
        }
    
        function handleRangeChange() {
            // console.log(this.value)
            if (this.name == 'playbackRate') {
                audio.playbackRate = parseFloat(this.value);
            } else if (this.name == 'volume') {
                audio.volume = parseFloat(this.value);
            }
        }
    
        function handleProgress(params) {
            const percent = Math.ceil((audio.currentTime / audio.duration) * 100)
            progressBar.style.flexBasis = `${percent}%`
            currentTime.innerHTML = formatTime( audio.currentTime );
            // console.log(percent)
        }
    
        handleProgress();
    
        function scrub(e) {
    
            // console.log(e.clientX)
            let offsetLeft = e.offsetX
            let width = progress.getBoundingClientRect().width
            let nowPos = (offsetLeft / width)
            // console.log(nowPos)
            audio.currentTime = audio.duration * nowPos
            console.log(audio.currentTime)
        }
    
    
        // Hook up event listeners
    
        // sliders
        Array.from(ranges).forEach(function (element) {
            element.addEventListener('change', handleRangeChange);
        });
    
        // audio events
        audio.addEventListener('click', togglePlay);
        audio.addEventListener('play', updateButton);
        audio.addEventListener('pause', updateButton);
        audio.addEventListener('timeupdate', handleProgress);
    
        toggle.addEventListener('click', togglePlay);
    
        mute.addEventListener('click', toggleSound);
    
        // window events
        window.addEventListener('keydown', function (event) {
            console.log(event.which)
            if (event.which == 32 || event.which == 179) {
                togglePlay();
            }
        })
    
        // progress bar
        let mousedown = false
        progress.addEventListener('click', scrub)
        progress.addEventListener('mousemove', function (e) {
            if (mousedown) {
                scrub(e)
            }
        })
        progress.addEventListener('mousedown', function () {
            mousedown = true
        })
        progress.addEventListener('mouseup', function () {
            mousedown = false
        })
    })
}
