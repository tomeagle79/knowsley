// $(document).ready( console.log('test from scroll js' ) );

$("a").on('click', function (event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
        // Prevent default anchor click behavior
        event.preventDefault();

        // Store hash
        var hash = this.hash;

        // Using jQuery's animate() method to add smooth page scroll
        // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 800, function () {

            // Add hash (#) to URL when done scrolling (default click behavior)
            window.location.hash = hash;
        });
    } // End if
});

// Make back to top button appear on scroll


//PARALLAX
var $document = jQuery(document);
var vp_height = jQuery(window).height();

var scrolled_distance = 0;

var $btt = $('.btn-back-top');
var $header = $('header');

//STICKY NAV AND PARRALAX HEADER
var sticknav = function(){
    if( /Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        return true;
    }
    if ( $(window).scrollTop() > 100 ) {
        $btt.addClass('show');
    } else {
        $btt.removeClass('show');
    };
}
jQuery(window).on('scroll', function() {
    window.requestAnimationFrame(sticknav);
});

window.requestAnimationFrame(sticknav);