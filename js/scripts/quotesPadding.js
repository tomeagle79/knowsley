// Add padding to section following quotes component
var $quotes = $('.quotes');

var isHome = $('body').hasClass("Home");

if( $quotes.length > 0 && !isHome ){
    
    $quotes.each(function(){
        var $parent = $(this).parents("section")
        var $nextSection = $parent.next()

        $nextSection.css("padding-top", "310px")
    })
}
