// Apply negative margin bottom to instagram section so it overlaps

var $instagram = $('.instagram-sotw-wrapper');

if( $instagram.length > 0 ){
    
    $instagram.each(function(){
        var $parent = $(this).parents("section")
        
        $parent.css("margin-bottom", "-35px")
    })
}