$(document).ready(function(){
    $("#dl-menu").dlmenu({
        onNavOpened: function(){
            $('html').addClass('modal-open');
            $('#dl-menu').addClass('visible');
        },
        onNavClosed: function(){
            $('html').removeClass('modal-open');
            $('#dl-menu').removeClass('visible');
        },
        triggerCustom:$(".js-menu-trigger"),
        animationClasses: {
            classin:"dl-animate-in-2",
            classout:"dl-animate-out-2"
        }
    });
   
});

