﻿[{
        "name": "Rich text editor",
        "alias": "rte",
        "view": "rte",
        "icon": "icon-article"
    },
    {
        "name": "Image",
        "alias": "media",
        "view": "media",
        "icon": "icon-picture"
    },
    {
        "name": "Macro",
        "alias": "macro",
        "view": "macro",
        "icon": "icon-settings-alt"
    },
    {
        "name": "Embed",
        "alias": "embed",
        "view": "embed",
        "icon": "icon-movie-alt"
    },
    {
        "name": "Quotes",
        "alias": "quoter",
        "view": "/App_Plugins/DocTypeGridEditor/Views/doctypegrideditor.html",
        "render": "/App_Plugins/DocTypeGridEditor/Render/DocTypeGridEditor.cshtml",
        "icon": "icon-quote",
        "config": {
            "allowedDocTypes": ["quoter"],
            "nameTemplate": "",
            "enablePreview": false,
            "viewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/",
            "previewViewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/Previews/",
            "previewCssFilePath": "",
            "previewJsFilePath": ""
        }
    },
    {
        "name": "Explore Slider",
        "alias": "exploreSlider",
        "view": "/App_Plugins/DocTypeGridEditor/Views/doctypegrideditor.html",
        "render": "/App_Plugins/DocTypeGridEditor/Render/DocTypeGridEditor.cshtml",
        "icon": "icon-slideshow",
        "config": {
            "allowedDocTypes": ["exploreSlider"],
            "nameTemplate": "",
            "enablePreview": false,
            "viewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/",
            "previewViewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/Previews/",
            "previewCssFilePath": "",
            "previewJsFilePath": ""
        }
    },
    {
        "name": "Simple Image Slider",
        "alias": "simpleImageSlider",
        "view": "/App_Plugins/DocTypeGridEditor/Views/doctypegrideditor.html",
        "render": "/App_Plugins/DocTypeGridEditor/Render/DocTypeGridEditor.cshtml",
        "icon": "icon-slideshow",
        "config": {
            "allowedDocTypes": ["simpleImageSlider"],
            "nameTemplate": "",
            "enablePreview": false,
            "viewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/",
            "previewViewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/Previews/",
            "previewCssFilePath": "",
            "previewJsFilePath": ""
        }
    },
    {
        "name": "Subhero component",
        "alias": "subHero",
        "view": "/App_Plugins/DocTypeGridEditor/Views/doctypegrideditor.html",
        "render": "/App_Plugins/DocTypeGridEditor/Render/DocTypeGridEditor.cshtml",
        "icon": "icon-item-arrangement",
        "config": {
            "allowedDocTypes": ["subHero"],
            "nameTemplate": "",
            "enablePreview": false,
            "viewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/",
            "previewViewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/Previews/",
            "previewCssFilePath": "",
            "previewJsFilePath": ""
        }
    },
    {
        "name": "Call to action component",
        "alias": "callToActionComponent",
        "view": "/App_Plugins/DocTypeGridEditor/Views/doctypegrideditor.html",
        "render": "/App_Plugins/DocTypeGridEditor/Render/DocTypeGridEditor.cshtml",
        "icon": "icon-mouse-cursor",
        "config": {
            "allowedDocTypes": ["callToActionComponent"],
            "nameTemplate": "",
            "enablePreview": false,
            "viewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/",
            "previewViewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/Previews/",
            "previewCssFilePath": "",
            "previewJsFilePath": ""
        }
    },
    {
        "name": "Animal call to action component",
        "alias": "animalCallToActionComponent",
        "view": "/App_Plugins/DocTypeGridEditor/Views/doctypegrideditor.html",
        "render": "/App_Plugins/DocTypeGridEditor/Render/DocTypeGridEditor.cshtml",
        "icon": "icon-mouse-cursor",
        "config": {
            "allowedDocTypes": ["animalCallToActionComponent"],
            "nameTemplate": "",
            "enablePreview": false,
            "viewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/",
            "previewViewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/Previews/",
            "previewCssFilePath": "",
            "previewJsFilePath": ""
        }
    },
    {
        "name": "Ticket call to action component",
        "alias": "ticletCallToActionComponent",
        "view": "/App_Plugins/DocTypeGridEditor/Views/doctypegrideditor.html",
        "render": "/App_Plugins/DocTypeGridEditor/Render/DocTypeGridEditor.cshtml",
        "icon": "icon-mouse-cursor",
        "config": {
            "allowedDocTypes": ["ticketCallToActionComponent"],
            "nameTemplate": "",
            "enablePreview": false,
            "viewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/",
            "previewViewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/Previews/",
            "previewCssFilePath": "",
            "previewJsFilePath": ""
        }
    },
    {
        "name": "Explore section",
        "alias": "exploreTeasers",
        "view": "/App_Plugins/DocTypeGridEditor/Views/doctypegrideditor.html",
        "render": "/App_Plugins/DocTypeGridEditor/Render/DocTypeGridEditor.cshtml",
        "icon": "icon-globe",
        "config": {
            "allowedDocTypes": ["exploreTeasers"],
            "nameTemplate": "",
            "enablePreview": false,
            "viewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/",
            "previewViewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/Previews/",
            "previewCssFilePath": "",
            "previewJsFilePath": ""
        }
    },
    {
        "name": "Accordion",
        "alias": "accordion",
        "view": "/App_Plugins/DocTypeGridEditor/Views/doctypegrideditor.html",
        "render": "/App_Plugins/DocTypeGridEditor/Render/DocTypeGridEditor.cshtml",
        "icon": "icon-list",
        "config": {
            "allowedDocTypes": ["accordionComponent"],
            "nameTemplate": "",
            "enablePreview": false,
            "viewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/",
            "previewViewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/Previews/",
            "previewCssFilePath": "",
            "previewJsFilePath": ""
        }
    },
    {
        "name": "Animal Sounds",
        "alias": "animalSounds",
        "view": "/App_Plugins/DocTypeGridEditor/Views/doctypegrideditor.html",
        "render": "/App_Plugins/DocTypeGridEditor/Render/DocTypeGridEditor.cshtml",
        "icon": "icon-sound",
        "config": {
            "allowedDocTypes": ["animalSounds"],
            "nameTemplate": "",
            "enablePreview": false,
            "viewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/",
            "previewViewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/Previews/",
            "previewCssFilePath": "",
            "previewJsFilePath": ""
        }
    },
    {
        "name": "Button builder",
        "alias": "buttonBuilder",
        "view": "/App_Plugins/DocTypeGridEditor/Views/doctypegrideditor.html",
        "render": "/App_Plugins/DocTypeGridEditor/Render/DocTypeGridEditor.cshtml",
        "icon": "icon-link",
        "config": {
            "allowedDocTypes": ["buttonBuilder"],
            "nameTemplate": "",
            "enablePreview": false,
            "viewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/",
            "previewViewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/Previews/",
            "previewCssFilePath": "",
            "previewJsFilePath": ""
        }
    },
    {
        "name": "Special Events and News Slider",
        "alias": "eventsAndNewsSlider",
        "view": "/App_Plugins/DocTypeGridEditor/Views/doctypegrideditor.html",
        "render": "/App_Plugins/DocTypeGridEditor/Render/DocTypeGridEditor.cshtml",
        "icon": "icon-newspaper",
        "config": {
            "allowedDocTypes": ["eventsAndNewsSlider"],
            "nameTemplate": "",
            "enablePreview": false,
            "viewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/",
            "previewViewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/Previews/",
            "previewCssFilePath": "",
            "previewJsFilePath": ""
        }
    },
    {
        "name": "Displays and Talks Slider",
        "alias": "displaysAndTalksSlider",
        "view": "/App_Plugins/DocTypeGridEditor/Views/doctypegrideditor.html",
        "render": "/App_Plugins/DocTypeGridEditor/Render/DocTypeGridEditor.cshtml",
        "icon": "icon-slideshow",
        "config": {
            "allowedDocTypes": ["displaysAndTalksSlider"],
            "nameTemplate": "",
            "enablePreview": false,
            "viewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/",
            "previewViewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/Previews/",
            "previewCssFilePath": "",
            "previewJsFilePath": ""
        }
    },
    {
        "name": "Newsletter Signup",
        "alias": "newsletter",
        "view": "/App_Plugins/DocTypeGridEditor/Views/doctypegrideditor.html",
        "render": "/App_Plugins/DocTypeGridEditor/Render/DocTypeGridEditor.cshtml",
        "icon": "icon-newspaper",
        "config": {
            "allowedDocTypes": ["newsletter"],
            "nameTemplate": "",
            "enablePreview": false,
            "viewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/",
            "previewViewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/Previews/",
            "previewCssFilePath": "",
            "previewJsFilePath": ""
        }
    },
    {
        "name": "Animal Slider",
        "alias": "animalSlider",
        "view": "/App_Plugins/DocTypeGridEditor/Views/doctypegrideditor.html",
        "render": "/App_Plugins/DocTypeGridEditor/Render/DocTypeGridEditor.cshtml",
        "icon": "icon-slideshow",
        "config": {
            "allowedDocTypes": ["animalSlider"],
            "nameTemplate": "",
            "enablePreview": false,
            "viewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/",
            "previewViewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/Previews/",
            "previewCssFilePath": "",
            "previewJsFilePath": ""
        }
    },
    // {
    //     "name": "Contact form",
    //     "alias": "contactForm",
    //     "view": "/App_Plugins/DocTypeGridEditor/Views/doctypegrideditor.html",
    //     "render": "/App_Plugins/DocTypeGridEditor/Render/DocTypeGridEditor.cshtml",
    //     "icon": "icon-mailbox",
    //     "config": {
    //         "allowedDocTypes": ["contactForm"],
    //         "nameTemplate": "",
    //         "enablePreview": false,
    //         "viewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/",
    //         "previewViewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/Previews/",
    //         "previewCssFilePath": "",
    //         "previewJsFilePath": ""
    //     }
    // },
    // {
    //     "name": "Video upload",
    //     "alias": "video",
    //     "view": "/App_Plugins/DocTypeGridEditor/Views/doctypegrideditor.html",
    //     "render": "/App_Plugins/DocTypeGridEditor/Render/DocTypeGridEditor.cshtml",
    //     "icon": "icon-video",
    //     "config": {
    //         "allowedDocTypes": ["videoUpload"],
    //         "nameTemplate": "",
    //         "enablePreview": false,
    //         "viewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/",
    //         "previewViewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/Previews/",
    //         "previewCssFilePath": "",
    //         "previewJsFilePath": ""
    //     }
    // }
]