//------------------------------------------------------------------------------
// <auto-generated>
//   This code was generated by a tool.
//
//    Umbraco.ModelsBuilder v3.0.10.102
//
//   Changes to this file will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.ModelsBuilder;
using Umbraco.ModelsBuilder.Umbraco;

namespace Umbraco.Web.PublishedContentModels
{
	/// <summary>General slide</summary>
	[PublishedContentModel("generalSlide")]
	public partial class GeneralSlide : PublishedContentModel
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "generalSlide";
		public new const PublishedItemType ModelItemType = PublishedItemType.Content;
#pragma warning restore 0109

		public GeneralSlide(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<GeneralSlide, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// Hero image alt text
		///</summary>
		[ImplementPropertyType("slideAltText")]
		public string SlideAltText
		{
			get { return this.GetPropertyValue<string>("slideAltText"); }
		}

		///<summary>
		/// Hero image
		///</summary>
		[ImplementPropertyType("slideImage")]
		public IPublishedContent SlideImage
		{
			get { return this.GetPropertyValue<IPublishedContent>("slideImage"); }
		}

		///<summary>
		/// Hero subtitle
		///</summary>
		[ImplementPropertyType("slideSubtitle")]
		public string SlideSubtitle
		{
			get { return this.GetPropertyValue<string>("slideSubtitle"); }
		}

		///<summary>
		/// Hero title
		///</summary>
		[ImplementPropertyType("slideTitle")]
		public IHtmlString SlideTitle
		{
			get { return this.GetPropertyValue<IHtmlString>("slideTitle"); }
		}
	}
}
