const mix = require('laravel-mix');
const path = '.';
const pathRes = require('path');

/*
https://webpack.js.org/configuration/devtool/
*/

mix.webpackConfig({
    devtool: "source-map",
    module: {
        rules: [
            {
                test: /\.modernizrrc.js$/,
                use: [ 'modernizr-loader' ]
            },
            {
                test: /\.modernizrrc(\.json)?$/,
                use: [ 'modernizr-loader', 'json-loader' ]
            }
        ]
    },
    resolve: {
        alias: {
            modernizr$: pathRes.resolve(__dirname, ".modernizrrc")
        }
    }
});

mix.setPublicPath('/');

mix.sass(`${path}/scss/style.scss`, `${path}/dist/css`)
    .sourceMaps()
    .options({
        autoprefixer: false,
        postCss: [
            require('autoprefixer')({
                remove: false,
                browsers: ['last 2 version', 'safari 5', 'ie 6', 'ie 7', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'ios 7', 'ios 8', 'ios 9', 'android 4'],
            }),
            require('postcss-flexbugs-fixes')(),
        ]
    });

mix.js([
        `${path}/js/app.js`,
    ], `${path}/dist/js/app.js`);
//
mix.browserSync({
    injectChanges: true,
    files: [
        `${path}/scss/**/*.scss`, 
        `${path}/js/**/*.js`, 
        `${path}/views/**/*.cshtml`
    ],
    logSnippet: true,
    proxy: 'http://localhost:37897/',
    port: 8080
})

//Because "dingaling dong" is annoying
mix.disableSuccessNotifications();